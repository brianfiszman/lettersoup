<?php

namespace App;

/**
 *
 */
class LetterSoupPatternLocalizer
{
    public function __construct()
    {
    }

    public static function fillLetterSoup($rows, $cols)
    {
        if ($rows == 0 || $cols == 0) {
            return null;
        }

        for ($i = 0; $i < $rows; $i++) {
            for ($j = 0; $j < $cols; $j++) {
                $lettersoup[$i][$j] = chr(rand(69, 79));
            }
        }

        return $lettersoup;
    }

    public static function createMatch(&$matches, &$lettersoup)
    {
        // If lettersoup is null we return
        if (is_null($lettersoup)) {
            return null;
        }


        // If there wasnt any match of any kind when looking for OIE we invent one.
        if ($matches['hAsc']['quantity'] == 0 && $matches['vAsc']['quantity'] == 0 && $matches['hDec']['quantity'] == 0 && $matches['vDec']['quantity'] == 0 && $matches['rDiagDec']['quantity'] == 0 && $matches['lDiagDec']['quantity'] == 0 && $matches['rDiagAsc']['quantity'] == 0 && $matches['lDiagAsc']['quantity'] == 0) {
            {
                if (sizeof($lettersoup[0]) > 2) {
                    $lettersoup[0][0] = 'O';
                    $lettersoup[0][1] = 'I';
                    $lettersoup[0][2] = 'E';
                    $matches['hAsc']['quantity']++;
                } elseif (sizeof($lettersoup) > 2) {
                    $lettersoup[0][0] = 'O';
                    $lettersoup[1][0] = 'I';
                    $lettersoup[2][0] = 'E';
                    $matches['vAsc']['quantity']++;
                }
            }
        }
        return $lettersoup;
    }

    // This function returns the amount of matches for the findPattern
    // If there was 3 OIE in horizontal or vertical sense, then $matches array would
    // count and store the amount of times there was a coincidense.
    public static function findPattern(&$lettersoup)
    {
        // To avoid errors in case of a null $lettersoup I return.
        if (is_null($lettersoup)) {
            return null;
        }



         // This array stores the amount of matches
         // from any angle and the position of every match
        $matches = [
            'hAsc'     => [
                'quantity'  => 0,
                'positions' => []
            ],
            'vAsc'     => [
                'quantity'  => 0,
                'positions' => []
            ],
            'hDec'     => [
                'quantity'  => 0,
                'positions' => []
            ],
            'vDec'     => [
                'quantity'  => 0,
                'positions' => []
            ],
            'rDiagAsc' => [
                'quantity'  => 0,
                'positions' => []
            ],
            'lDiagAsc' => [
                'quantity'  => 0,
                'positions' => []
            ],
            'rDiagDec' => [
                'quantity'  => 0,
                'positions' => []
            ],
            'lDiagDec' => [
                'quantity'  => 0,
                'positions' => []
            ],
        ];

        // We cycle through the lettersoup looking for matches
        for ($i = 0; $i < sizeof($lettersoup); $i++) {
            for ($j = 0; $j < sizeof($lettersoup[$i]); $j++) {

                // Horizontal
                if ($j < sizeof($lettersoup[$i]) - 2) {
                    if ($lettersoup[$i][$j] == 'O' &&  $lettersoup[$i][$j + 1] == 'I' &&  $lettersoup[$i][$j + 2] == 'E') {
                        // We have a match!
                        // Everytime there is a match, we add 1 to the counter
                        // and store the position for this sense
                        $matches['hAsc']['quantity']++;
                        array_push($matches['hAsc']['positions'], [[$i, $j]]);
                    }
                }


                if ($j > 1) {
                    if ($lettersoup[$i][$j] == 'O' &&  $lettersoup[$i][$j - 1] == 'I' &&  $lettersoup[$i][$j - 2] == 'E') {
                        // Same sense but backwards
                        $matches['hDec']['quantity']++;
                        array_push($matches['hDec']['positions'], [[$i, $j]]);
                    }
                }

                // Vertical
                if ($i < sizeof($lettersoup) - 2) {
                    if ($lettersoup[$i][$j] == 'O' &&  $lettersoup[$i + 1][$j] == 'I' &&  $lettersoup[$i + 2][$j] == 'E') {
                        $matches['vAsc']['quantity']++;
                        array_push($matches['vAsc']['positions'], [[$i, $j]]);
                    }
                }

                if ($i > 1) {
                    if ($lettersoup[$i][$j] == 'O' &&  $lettersoup[$i - 1][$j] == 'I' &&  $lettersoup[$i - 2][$j] == 'E') {
                        $matches['vDec']['quantity']++;
                        array_push($matches['vDec']['positions'], [[$i, $j]]);
                    }
                }

                // Diagonal to the Right
                if ($j < sizeof($lettersoup[$i]) - 2 && $i < sizeof($lettersoup) - 2) {
                    if ($lettersoup[$i][$j] == 'O' &&  $lettersoup[$i + 1][$j + 1] == 'I' &&  $lettersoup[$i + 2][$j + 2] == 'E') {
                        $matches['rDiagAsc']['quantity']++;
                        array_push($matches['rDiagAsc']['positions'], [[$i, $j]]);
                    }
                }

                if ($i > 1 && $j > 1) {
                    if ($lettersoup[$i][$j] == 'O' &&  $lettersoup[$i - 1][$j - 1] == 'I' &&  $lettersoup[$i - 2][$j - 2] == 'E') {
                        $matches['rDiagDec']['quantity']++;
                        array_push($matches['rDiagDec']['positions'], [[$i, $j]]);
                    }
                }

                // Diagonal to the Left
                if ($j > 1 && $i < sizeof($lettersoup) - 2) {
                    if ($lettersoup[$i][$j] == 'O' &&  $lettersoup[$i + 1][$j - 1] == 'I' &&  $lettersoup[$i + 2][$j - 2] == 'E') {
                        $matches['lDiagAsc']['quantity']++;
                        array_push($matches['lDiagAsc']['positions'], [[$i, $j]]);
                    }
                }

                if ($i > 1 && $j < sizeof($lettersoup[$i]) - 2) {
                    if ($lettersoup[$i][$j] == 'O' &&  $lettersoup[$i - 1][$j + 1] == 'I' &&  $lettersoup[$i - 2][$j + 2] == 'E') {
                        $matches['lDiagDec']['quantity']++;
                        array_push($matches['lDiagDec']['positions'], [[$i, $j]]);
                    }
                }
            }
        }

        return $matches;
    }
}
