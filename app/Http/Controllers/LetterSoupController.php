<?php

namespace App\Http\Controllers;

use App\LetterSoupPatternLocalizer;
use App\User;
use Illuminate\Http\Request;
use Storage;
use Response;

class LetterSoupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     // I used this function as a dummy
    public function index()
    {
        for ($i = 0; $i < 100; $i++) {
            for ($j = 0; $j < 100; $j++) {
                $lettersoup[$i][$j] = chr(rand(69, 79));
            }
        }

        return LetterSoupPatternLocalizer::findPattern($lettersoup);
    }

    public function show($rows, $cols)
    {
        // Fill the lettersoup
        $lettersoup = LetterSoupPatternLocalizer::fillLetterSoup($rows, $cols);

        // If its null...
        if (is_null($lettersoup)) {
            return "<h1>Insert a valid URL</h1>";
        }

        // Find pattern matches
        $matches    = LetterSoupPatternLocalizer::findPattern($lettersoup);

        // This function creates puts O I E in the array in case no pattern is found
        $lettersoup = LetterSoupPatternLocalizer::createMatch($matches, $lettersoup);

        return view('index', [
            'lettersoup'   => json_encode($lettersoup),
            'coincidences' => $matches
        ]);
    }
}
