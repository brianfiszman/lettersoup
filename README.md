# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

Open an terminal (preferably with a bash shell) and follow the steps.

First clone the project:
$> git clone https://bitbucket.org/brianfiszman/lettersoup

Enter the project root folder.
$> cd ./lettersoup

Install composer dependencies:
$> composer install

In the terminal go to the project root and input the following:
$> cp .env.example .env

Generate the key of the project:
$> php artisan key:generate

Serve!
$> php artisan serve

Also, when not "serving" you can use the test the project with unit testing.
You need PHP Unit installed.

If you have PHP Unit Installed... Test!
$> phpunit

If you want to use the application, enter the browser, serve the application and go to:
https://127.0.0.1:8000/15/15

(I input 15 but you can input any number and it will be fine as long as its not a huge value like 3000)