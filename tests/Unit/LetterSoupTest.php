<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\LetterSoupPatternLocalizer;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LetterSoupTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFillSoup()
    {

        $lettersoup = LetterSoupPatternLocalizer::fillLetterSoup(0, 0);

        // Checks if the Soup is null
        $this->assertTrue(is_null($lettersoup));

        // Checks if the Soup is not null
        $lettersoup = LetterSoupPatternLocalizer::fillLetterSoup(20, 20);
        $this->assertTrue(!is_null($lettersoup));

        //Checks If The Soup is filled with the right amount of rows and cols
        $this->assertTrue(sizeof($lettersoup)    == 20);
        $this->assertTrue(sizeof($lettersoup[0]) == 20);

        $lettersoup = LetterSoupPatternLocalizer::fillLetterSoup(123, 321);
        $this->assertTrue(sizeof($lettersoup)    == 123);
        $this->assertTrue(sizeof($lettersoup[0]) == 321);
    }

    public function testMatches()
    {
        // Checks if there is at least one match if there is just a row
        $lettersoup = LetterSoupPatternLocalizer::fillLetterSoup(1, 3);
        $matches    = LetterSoupPatternLocalizer::findPattern($lettersoup);
        $lettersoup = LetterSoupPatternLocalizer::createMatch($matches, $lettersoup);

        $this->assertTrue($matches['hAsc']['quantity'] > 0);

        // Checks if there is at least one match if there is just a column
        $lettersoup = LetterSoupPatternLocalizer::fillLetterSoup(3, 1);
        $matches    = LetterSoupPatternLocalizer::findPattern($lettersoup);
        $lettersoup = LetterSoupPatternLocalizer::createMatch($matches, $lettersoup);

        $this->assertTrue($matches['vAsc']['quantity'] > 0);

        // Checks if there is not even one match if the args are invalid
        $lettersoup = LetterSoupPatternLocalizer::fillLetterSoup(0, 3);
        $matches    = LetterSoupPatternLocalizer::findPattern($lettersoup);
        $lettersoup = LetterSoupPatternLocalizer::createMatch($matches, $lettersoup);
        $this->assertTrue($matches['hAsc']['quantity'] == 0);

        // Checks if there is not even one match if the args are invalid
        $lettersoup = LetterSoupPatternLocalizer::fillLetterSoup(3, 0);
        $matches    = LetterSoupPatternLocalizer::findPattern($lettersoup);
        $lettersoup = LetterSoupPatternLocalizer::createMatch($matches, $lettersoup);

        $this->assertTrue($matches['vAsc']['quantity'] == 0);
    }
}
